This project is created for pre school management app

## Scalability Factor

## Features

* [Redux](http://redux.js.org/)
* [Redux Saga](https://redux-saga.js.org/)
* [Redux Persist](https://github.com/rt2zz/redux-persist/)
* [Redux Logger](https://github.com/LogRocket/redux-logger/)
* [React Navigation](https://reactnavigation.org/)
* [React Native Gesture Handler](https://github.com/kmagiera/react-native-gesture-handler) 
* [axios](https://github.com/axios/axios)
* [lodash](https://lodash.com)

## Dev tools
* [Eslint](http://eslint.org/) ([Airbnb config](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb))
* [Jest](https://facebook.github.io/jest/)
* [axios-mock-adapter](https://github.com/ctimmerm/axios-mock-adapter)
* [Remote Redux DevTools](https://github.com/ctimmerm/axios-mock-adapter)



## Project Structure
- `/app` - Contains our React Native App codebase
  - `/api` - Api module class. Each file will be feature based like ProductAPI, LoginAPI etc
  - `/components` - universal shared components used by others.
  - `/config` - Universal styles,images,metrics etc..
  - `/lib` - lib helper files
  - `/navigation` - All naviagtion related studd including helpers functions and navigation stack
    - `NavigationHelpers.js` -  Use this class instead of depending on props.naviagtion
    - `NavigationService.js` - Service class for naviagtion
    - `NavigationStack.js` - Stack to define naviagtion. you can split things further if needed
  - `/store` - Includes everything you need to set up store. 
    - `configureStore` - Combines all feature based sagas




# react-native-preschool-demo


