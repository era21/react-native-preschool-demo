/* Redux saga class
 * logins the user into the app
 * requires username and password.
 * un - username
 * pwd - password
 */
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import { Alert } from 'react-native';
import { requestOTP } from '../api/methods/Auth';
import * as loginActions from '../redux/actions/loginActions';
import * as navigationActions from '../redux/actions/navigationActions';

// Our worker Saga that logins the user
export default function* loginAsync() {
    yield put(loginActions.enableLoader());

    const response = {};

    try {
        //how to call api
        // const response = yield call(loginUser, action.username, action.password);
        response = yield call(requestOTP, action.payload);
    } catch (error) {
        yield put({ type: 'ERROR' });
    }

    if (response.success) {
        yield put(loginAction.requestLogin(response.data));
        yield put(loginActions.disableLoader({}));
        yield call(navigationActions.navigateToHome);
    } else {
        yield put(loginActions.loginFailed());
        yield put(loginActions.disableLoader({}));
        setTimeout(() => {
            Alert.alert('BoilerPlate', response.Message);
        }, 200);
    }
}
