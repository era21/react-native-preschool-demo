/*
 * Provides universal fonts used in the app.
 */
import { StyleSheet } from 'react-native';
import { Colors, FontSize } from '../../config/';

const Typography = StyleSheet.create({
  largerHeading: {
    fontSize: FontSize.largeText,
    fontWeight: 'bold',
    color: Colors.TEXT_PRIMARY_COLOR
  },
  heading: {
    fontSize: FontSize.h1,
    fontWeight: 'bold',
    color: Colors.TEXT_PRIMARY_COLOR
  },

  title: {
    fontSize: FontSize.h2,
    fontWeight: 'bold',
    color: Colors.TEXT_PRIMARY_COLOR
  },

  subtitle: {
    fontSize: FontSize.h3,
    fontWeight: 'normal',
    color: Colors.TEXT_PRIMARY_COLOR
  },

  caption: {
    fontSize: FontSize.title,
    fontWeight: 'bold',
    color: Colors.COLOR_TEXT_SECONDARY_GREY
  },
  text: {
    fontSize: FontSize.body,
    fontWeight: 'normal',
    color: Colors.COLOR_TEXT_SECONDARY_GREY
  },
  link: {
    fontSize: FontSize.body,
    fontWeight: 'bold',
    color: Colors.COLOR_SUPPORT_BLUE
  },
  textSmall: {
    fontSize: FontSize.bodySmall,
    fontWeight: 'normal',
    color: Colors.uiOne
  }
});

export default Typography;
