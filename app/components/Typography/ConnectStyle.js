import React from 'react';
import Styles from './Styles';
import { DEFAULT_FONT } from './TypeFace';

const translateProps = (props, componentDisplayName) => {
  const { weight, align, style } = props;
  let changeStyleOnFly = {
    ...style,
    fontWeight: setWeight(weight),
    textAlign: align ? align : 'left'
    // fontFamily: DEFAULT_FONT
  };

  switch (componentDisplayName) {
    case 'heading':
      return { ...props, style: [Styles.heading, changeStyleOnFly] };
    case 'title':
      return { ...props, style: [Styles.title, changeStyleOnFly] };
    case 'subtitle':
      return { ...props, style: [Styles.subtitle, changeStyleOnFly] };
    case 'caption':
      return { ...props, style: [Styles.caption, changeStyleOnFly] };
    case 'text':
      return { ...props, style: [Styles.text, changeStyleOnFly] };
    case 'link':
      return { ...props, style: [Styles.link, changeStyleOnFly] };
    case 'display':
      return { ...props, style: [Styles.largerHeading, changeStyleOnFly] };
    default:
      return props;
  }
};

export default (
  componentStyleName,
  componentStyle = {},
  mapPropsToStyleNames,
  options = {}
) => {
  return function wrapWithStyledComponent(WrappedComponent) {
    return function wrappedRender(args) {
      return <WrappedComponent {...translateProps(args, componentStyleName)} />;
    };
  };
};

const setWeight = weight => {
  switch (weight) {
    case 'light':
      return '300';
    case 'regular':
      return '500';
    case 'bold':
      return '900';
    default:
      return weight;
  }
};
