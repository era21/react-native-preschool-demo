import React, { Component } from 'react';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';
import { Display, Heading, Title } from '../../components/Typography';
import { Colors } from '../../config';
import NavigationService from '../../navigation/NavigationService';

class HomeContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Display align="center" weight="bold">
          👋 {'\n'}Hey there
        </Display>
        <Heading colo weight="light" align="center">
          say Hi, to {'\n'}
          <Title weight="100" align="center">
            React Native Preloaded {'\n'}Kick Starter Kit
          </Title>
        </Heading>

        <Button
          title="Next"
          onPress={() => NavigationService.navigate('Login', {})}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: Colors.SCREEN_BACKGROUND_COLOR,
    flex: 1,
    justifyContent: 'center'
  }
});

function mapStateToProps() {
  return {};
}
function mapDispatchToProps() {
  return {};
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer);
