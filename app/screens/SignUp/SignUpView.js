import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import NavigationService from '../../navigation/NavigationService';

class SignUpView extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Login</Text>
        <TouchableOpacity onPress={() => NavigationService.goBack()}>
          <Text>Go to Home</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

SignUpView.propTypes = {
  onSignUp: PropTypes.func
};

export default SignUpView;
