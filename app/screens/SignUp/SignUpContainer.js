import React, { Component } from 'react';
import SignUpView from './SignUpView';
import { connect } from 'react-redux';
import * as loginActions from '../../redux/actions/loginActions';

class SignUpContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <SignUpView {...this.props} />;
    }
}

function mapStateToProps() {
    return {};
}
function mapDispatchToProps(dispatch) {
    return {
        onSignUp: (un, pwd) => dispatch(loginActions.requestLogin(un, pwd))
    };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignUpContainer);
