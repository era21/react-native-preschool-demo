import { createStackNavigator } from 'react-navigation';
import SignUp from '../screens/SignUp';
import Login from '../screens/Login';
import Home from '../screens/Home';

const RNApp = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: { gesturesEnabled: false, headerTransparent: true },
      headerTransparent: true
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: { gesturesEnabled: false, headerTransparent: true },
      headerTransparent: true
    },
    Home: {
      screen: Home,
      navigationOptions: {
        title: 'React Native Preloaded',
        gesturesEnabled: false,
        headerTransparent: true
      }
    }
  },
  {
    initialRouteName: 'Home'
  }
);

export default RNApp;
