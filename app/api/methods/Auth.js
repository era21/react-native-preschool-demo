import Api from '../';
import { ApiEndpoint, Method } from '../ApiConstants';

/**
  API request signature (URL, method, token, params, data)
 */

export function requestOTP(data) {
    return Api(ApiEndpoint.OTP_REQUEST, Method.POST, null, null, data)({});
}