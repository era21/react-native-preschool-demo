/* App config for apis
 */

export const Method = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
};


export const ApiEndpoint = {
    BASE_URL: 'http://www.some.site', //DEV
    BASE_URL: 'http://www.some.production', //Production
    

    //Auth
    OTP_REQUEST: '/api/uaa/otp/request',

};


export const ApiResponse = {
    authResponseMessage: {
        OTP_SEND_SUCCESS: 'otp_success'
    }
}
